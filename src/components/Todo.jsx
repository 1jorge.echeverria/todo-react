const Todo = ({ todo, deleteTodo, updateTodo }) => {
    return (
        <li className="list-group-item">
            <div className="d-flex justify-content-between align-items-start">
                <div>
                    <h5 className={`${todo.state && 'text-decoration-line-through'}`}>{todo.title}</h5>
                    <p className={`${todo.state && 'text-decoration-line-through'}`}>{todo.description}</p>
                    <div className="d-flex gap-2">
                        <button onClick={() => deleteTodo(todo.id)} className="btn btn-sm btn-danger">Eliminar</button>
                        <button onClick={() => updateTodo(todo.id)} className="btn btn-sm btn-warning">Actualizar</button>
                    </div>
                </div>
                <span className="badge text-bg-primary">{todo.priority && 'Prioritario'}</span>
            </div>
        </li >
    );
}

export default Todo;