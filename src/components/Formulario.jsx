import { useState } from "react";
import Swal from 'sweetalert2';

const Formulario = ({ addTodo }) => {

    const [todo, setTodo] = useState({
        title: 'Todo #01',
        description: 'Description #01',
        state: 'pendiente',
        priority: true,
    });

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!todo.title.trim() || !todo.description.trim()) {
            return Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "El título y la descripción son obligatorios!",
            });
        }

        addTodo({
            id: Date.now(),
            ...todo,
            state: todo.state === 'completado'
        })

        Swal.fire({
            position: "center",
            icon: "success",
            title: "ToDo agregado exitosamente!",
            showConfirmButton: false,
            timer: 1500
        });
    }

    const handleChange = (e) => {
        setTodo({
            ...todo,
            [e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value,
        })
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                placeholder="Ingrese Todo"
                className="form-control mb-2"
                name="title"
                value={todo.title}
                onChange={handleChange}
            />
            <textarea
                className="form-control mb-2"
                placeholder="Ingrese Descripción"
                name="description"
                value={todo.description}
                onChange={handleChange}
            />
            <div className="form-check">
                <input
                    type="checkbox"
                    name="priority"
                    className="form-check-input"
                    id="inputCheck"
                    checked={todo.priority}
                    onChange={handleChange}
                />
                <label htmlFor="inputCheck">Prioritario</label>
            </div>
            <select
                className="form-select mb-2"
                name="state" value={todo.state}
                onChange={handleChange}
            >
                <option value="pendiente">Pendiente</option>
                <option value="completado">Completado</option>
            </select>
            <button type="submit" className="btn btn-primary">Agregar ToDo</button>
        </form>
    );
};

export default Formulario;