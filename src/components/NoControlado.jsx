import { useRef, useState } from "react";

const NoControlado = () => {

    const form = useRef(null);
    const [error, setError] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        setError('');

        //capturar datos
        const { title, description, state } = Object.fromEntries([...(new FormData(form.current)).entries()]);

        //validar datos
        if (!title.trim() || !description.trim() || !state.trim()) {
            return setError("Llena todos los campos");
        }

        //enviar datos
        console.log(title, description, state);
    }

    return (
        <form onSubmit={handleSubmit} ref={form}>
            <input type="text" placeholder="Ingrese Tarea" className="form-control mb-2" name="title" />
            <textarea className="form-control mb-2" placeholder="Ingrese Descripción" name="description"></textarea>
            <select className="form-select mb-2" name="state" defaultValue="completado">
                <option value="pendiente">Pendiente</option>
                <option value="completado">Completado</option>
            </select>
            <button type="submit" className="btn btn-primary">Procesar</button>
            {
                error !== '' && error
            }
        </form>
    );
};

export default NoControlado;