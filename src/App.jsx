import { useEffect, useState } from "react";
import Formulario from "./components/Formulario";
import Todos from "./components/Todos";

/*
const initialStateTodos = [
  {
    id: 1,
    title: 'Todo #01',
    description: 'Description #01',
    state: true,
    priority: true,
  },
  {
    id: 2,
    title: 'Todo #01',
    description: 'Description #01',
    state: false,
    priority: true,
  },
  {
    id: 3,
    title: 'Todo #03',
    description: 'Description #03',
    state: false,
    priority: true,
  },
];
*/

const initialStateTodos = JSON.parse(localStorage.getItem("todos")) || [];

const App = () => {

  const [todos, setTodos] = useState(initialStateTodos);

  useEffect(() => localStorage.setItem('todos', JSON.stringify(todos)), [todos]);

  const addTodo = (todo) => setTodos([...todos, todo]);

  const deleteTodo = (id) => setTodos(todos.filter(todo => todo.id !== id));

  const updateTodo = (id) => {
    setTodos(todos.map(todo => {
      if (todo.id === id) {
        todo.state = !todo.state;
      }
      return todo;
    }));
  }

  const orderTodo = arrayTodos => {
    return arrayTodos.sort((a, b) => {
      if (a.priority === b.priority) return 0;
      if (a.priority) return -1;
      if (!a.priority) return 1;
    })
  };

  return (
    <div className="container mb-2">
      <h1 className="my-5">Formulario</h1>
      <Formulario addTodo={addTodo}></Formulario>
      <Todos
        todos={orderTodo(todos)}
        deleteTodo={deleteTodo}
        updateTodo={updateTodo}
      />
    </div>
  );
};

export default App;